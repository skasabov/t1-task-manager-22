package ru.t1.skasabov.tm.enumerated;

import ru.t1.skasabov.tm.comparator.CreatedComparator;
import ru.t1.skasabov.tm.comparator.NameComparator;
import ru.t1.skasabov.tm.comparator.StatusComparator;
import ru.t1.skasabov.tm.exception.field.SortIncorrectException;
import ru.t1.skasabov.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    private final String name;

    private final Comparator<Task> comparator;

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        throw new SortIncorrectException();
    }

    TaskSort(final String name, final Comparator<Task> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

    public String getName() {
        return name;
    }

    public Comparator<Task> getComparator() {
        return comparator;
    }

}
