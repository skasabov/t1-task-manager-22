package ru.t1.skasabov.tm.service;

import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.api.service.ITaskService;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.TaskNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository>
        implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(
            final String userId,
            final String name,
            final String description,
            final Date dateBegin,
            final Date dateEnd
    ) {
        final Task task = create(name, description);
        if (task == null) throw new TaskNotFoundException();
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public void updateById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void updateByIndex(
            final String userId,
            final Integer index,
            final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void changeTaskStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
    }

    @Override
    public void changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize(userId)) throw new IndexIncorrectException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

}
