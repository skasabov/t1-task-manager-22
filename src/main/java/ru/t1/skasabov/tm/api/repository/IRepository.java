package ru.t1.skasabov.tm.api.repository;

import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M add(M model);

    Boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    int getSize();

    M removeOne(M model);

    void removeOneById(String id);

    void removeOneByIndex(Integer index);

    void removeAll(Collection<M> collection);

    void removeAll();

}
