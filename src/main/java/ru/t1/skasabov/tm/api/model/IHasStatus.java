package ru.t1.skasabov.tm.api.model;

import ru.t1.skasabov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
