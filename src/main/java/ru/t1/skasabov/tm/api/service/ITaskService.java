package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    Task create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAllByProjectId(String userId, String projectId);

    void updateById(String userId, String id, String name, String description);

    void updateByIndex(String userId, Integer index, String name, String description);

    void changeTaskStatusById(String userId, String id, Status status);

    void changeTaskStatusByIndex(String userId, Integer index, Status status);

}
