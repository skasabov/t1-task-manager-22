package ru.t1.skasabov.tm.repository;

import ru.t1.skasabov.tm.api.repository.IUserOwnedRepository;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void removeAll(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public Boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty() || id == null || id.isEmpty()) return null;
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public Integer getSize(final String userId) {
        return (int) records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public M removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty() || id == null || id.isEmpty()) return null;
        final M model = findOneById(userId, id);
        if (model == null) throw new ModelEmptyException();
        return removeOne(model);
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) throw new ModelEmptyException();
        removeOne(model);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty() || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public M removeOne(final String userId, final M model) {
        if (userId == null || userId.isEmpty() || model == null) return null;
        return removeOneById(userId, model.getId());
    }

}
