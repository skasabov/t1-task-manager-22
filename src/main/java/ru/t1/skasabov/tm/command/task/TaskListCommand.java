package ru.t1.skasabov.tm.command.task;

import ru.t1.skasabov.tm.enumerated.TaskSort;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    private static final String NAME = "task-list";

    private static final String DESCRIPTION = "Show list tasks.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final TaskSort sort = TaskSort.toSort(sortType);
        final String userId = getUserId();
        final boolean checkSort = sort == null;
        final Comparator<Task> comparator = (checkSort ? null : sort.getComparator());
        final List<Task> tasks = getTaskService().findAll(userId, comparator);
        renderTasks(tasks);
    }

}
