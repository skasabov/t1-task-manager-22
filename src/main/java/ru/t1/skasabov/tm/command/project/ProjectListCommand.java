package ru.t1.skasabov.tm.command.project;

import ru.t1.skasabov.tm.enumerated.ProjectSort;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    private static final String NAME = "project-list";

    private static final String DESCRIPTION = "Show all projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final ProjectSort sort = ProjectSort.toSort(sortType);
        final String userId = getUserId();
        final boolean checkSort = sort == null;
        final Comparator<Project> comparator = (checkSort ? null : sort.getComparator());
        final List<Project> projects = getProjectService().findAll(userId, comparator);
        renderProjects(projects);
    }

}
